package w.in.kycdemoapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {


  private Button pan_card_upload;
  private Button adhaar_card_upload;
  private FrameLayout frame_layout;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    pan_card_upload = (Button) findViewById(R.id.pan_card_upload);
    adhaar_card_upload = (Button) findViewById(R.id.adhaar_card_upload);
    frame_layout = (FrameLayout) findViewById(R.id.frame_layout);


    if(!ResourcesUtil.isConnectedToInternet(MainActivity.this)){

      Toast.makeText(this, "Please Connect to Internet", Toast.LENGTH_SHORT).show();

    }


    setFragment(new PancardFragment(), 1);

    pan_card_upload.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        setFragment(new PancardFragment(),0);

      }
    });

    adhaar_card_upload.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        setFragment(new AdhaarcardFragment(),0);
      }
    });
  }


  protected void setFragment(Fragment fragment, int i) {
    FragmentTransaction t = getSupportFragmentManager().beginTransaction();
    if (i == 1)
      t.add(R.id.frame_layout, fragment);
    t.replace(R.id.frame_layout, fragment);
    t.commit();
  }

}
