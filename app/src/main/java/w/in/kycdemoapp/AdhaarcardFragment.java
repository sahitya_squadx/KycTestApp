package w.in.kycdemoapp;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.ParsedRequestListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.util.HashMap;
import java.util.Map;

import w.in.kycdemoapp.xmltojson.XmlToJson;


/**
 * Created by sahitya on 16/4/18.
 */

public class AdhaarcardFragment extends Fragment {


  private final int REQUEST_CODE_SCAN = 101;
  private Button start_scan_button;
  private EditText et_aadhar_number;
  private EditText et_address_line_two;
  private EditText et_address_line_one;
  private EditText et_city;
  private EditText et_state;
  private EditText et_pin_code;
  private Button btn_aadhar_submit;
  private String[] permissions;
  private Object _user_name;
  private Object _gender;
  private Object _user_district;
  private Object _user_village_town;
  private Object _user_dob;
  private String _sub_distric;
  private String _user_state;
  private String _user_address;
  private String _pan_number;
  private String _district;
  private String _addhaar_num;
  private String _pincode;

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.adhaarcard_fragment_layout, container, false);
    permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};


    et_aadhar_number = (EditText) view.findViewById(R.id.et_aadhar_number);
    et_address_line_two = (EditText) view.findViewById(R.id.et_address_line_two);
    et_address_line_one = (EditText) view.findViewById(R.id.et_address_line_one);
    et_city = (EditText) view.findViewById(R.id.et_city);
    et_state = (EditText) view.findViewById(R.id.et_state);
    et_pin_code = (EditText) view.findViewById(R.id.et_pin_code);

    start_scan_button = (Button) view.findViewById(R.id.start_scan_button);
    btn_aadhar_submit = (Button) view.findViewById(R.id.btn_aadhar_submit);

    start_scan_button.setOnClickListener(v -> {
      Intent intent = new Intent(getActivity(), ScanActivity.class);
      startActivityForResult(intent, REQUEST_CODE_SCAN);
    });

    btn_aadhar_submit.setOnClickListener(v -> {


      String adhaar_number = et_aadhar_number.getText().toString();
      String address = et_address_line_one.getText().toString();
      String address2 = et_address_line_two.getText().toString();
      String city = et_city.getText().toString();
      String state = et_state.getText().toString();
      String pincode = et_pin_code.getText().toString();


      if (adhaar_number != null && !adhaar_number.isEmpty()) {


        if (adhaar_number.length() == 12) {
          if (address != null && !address.isEmpty()) {


            if (city != null && !city.isEmpty()) {

              if (state != null && !state.isEmpty()) {


                if (pincode != null && !pincode.isEmpty()) {

                  if (ResourcesUtil.isConnectedToInternet(getActivity())) {
                    save_user_data_from_server(adhaar_number, address, city, state, pincode);
                  } else {
                    Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
                  }

                } else {
                  et_pin_code.setError("Pincode cat be empty");
                  Toast.makeText(getActivity(), "Enter Picode", Toast.LENGTH_SHORT).show();
                }

              } else {
                et_state.setError("State cat be empty");
                Toast.makeText(getActivity(), "Enter State", Toast.LENGTH_SHORT).show();
              }

            } else {
              et_city.setError("City cat be empty");
              Toast.makeText(getActivity(), "Enter City", Toast.LENGTH_SHORT).show();
            }

          } else {
            et_address_line_one.setError("Address cat be empty");
            Toast.makeText(getActivity(), "Enter address!", Toast.LENGTH_SHORT).show();
          }


        } else {
          Toast.makeText(getActivity(), "Invalid Adhaar Number", Toast.LENGTH_SHORT).show();
        }


      } else {

        et_aadhar_number.setError("Adhaar Number cat be empty");
        Toast.makeText(getActivity(), "Enter Adhaar Number", Toast.LENGTH_SHORT).show();
      }


    });

    return view;
  }

  private void save_user_data_from_server(String adhaar_number, String address, String city, String state, String pincode) {

    ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
    progressDialog.setCancelable(false);
    progressDialog.show();


    Map<String, Object> bodyParamter = new HashMap<>();
    bodyParamter.put("adhaar_number", adhaar_number);
    bodyParamter.put("user_name", get_user_name());
    bodyParamter.put("user_address", address);
    bodyParamter.put("user_city", city);
    bodyParamter.put("user_state", state);
    bodyParamter.put("user_pincode", pincode);
    bodyParamter.put("user_gender", get_gender());
    bodyParamter.put("user_district", get_user_district());
    bodyParamter.put("user_village_town", get_user_village_town());
    bodyParamter.put("user_dob", get_user_dob());
    bodyParamter.put("user_id", ResourcesUtil.taking_device_id() + "");


    if (bodyParamter != null) {

      AndroidNetworking.post("http://ec2-34-224-102-217.compute-1.amazonaws.com:3000/userAdd/adhaarcard")
              .addBodyParameter(bodyParamter)
              .setContentType("x-www-form-urlencoded")
              .setPriority(Priority.HIGH)
              .build()
              .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {

                  Log.d("Response ::", " ---> " + response);


                  if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();

                  try {
                    String status = response.has("status") ? response.getString("status") : "0";
                    String message = response.has("message") ? response.getString("message") : "";

                    if (status.contentEquals("1")) {
                      Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    } else {
                      Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    }

                  } catch (JSONException e) {
                    e.printStackTrace();
                  }

                }

                @Override
                public void onError(ANError anError) {

                  if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                  Toast.makeText(getActivity(), "Error:: " + anError.getMessage(), Toast.LENGTH_SHORT).show();

                }
              });

    }


  }


  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);

    if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_SCAN) {
      String data_from = data.getStringExtra("received_text");
      Log.d("Data from ::: ", " Data from Adhaar Card ::: " + data_from);

      XmlToJson xmlToJsonRespnose = new XmlToJson.Builder(data_from).build();

      JSONObject jsonObject = xmlToJsonRespnose.toJson();

      Log.d("Xml to Json", "--===-->>> " + jsonObject.toString());


      try {

        if (jsonObject.has("PrintLetterBarcodeData")) {
          JSONObject printted_letterbarcode_data = jsonObject.has("PrintLetterBarcodeData") ? jsonObject.getJSONObject("PrintLetterBarcodeData") : null;
          set_sub_distric(printted_letterbarcode_data.has("subdist") ? printted_letterbarcode_data.getString("subdist") : "");
          set_user_district(printted_letterbarcode_data.has("subdist") ? printted_letterbarcode_data.getString("subdist") : "");
          set_user_dob(printted_letterbarcode_data.has("dob") ? printted_letterbarcode_data.getString("dob") : "");
          set_user_name(printted_letterbarcode_data.has("name") ? printted_letterbarcode_data.getString("name") : "");
          set_user_state(printted_letterbarcode_data.has("state") ? printted_letterbarcode_data.getString("state") : "");
          set_user_address(printted_letterbarcode_data.has("house") ? printted_letterbarcode_data.getString("house") : "");
          set_gender(printted_letterbarcode_data.has("gender") ? printted_letterbarcode_data.getString("gender") : "");
          set_addhaar_num(printted_letterbarcode_data.has("uid") ? printted_letterbarcode_data.getString("uid") : "");
          set_district(printted_letterbarcode_data.has("dist") ? printted_letterbarcode_data.getString("dist") : "");
          set_user_village_town(printted_letterbarcode_data.has("vtc") ? printted_letterbarcode_data.getString("vtc") : "");
          set_pincode(printted_letterbarcode_data.has("pc") ? printted_letterbarcode_data.getString("pc") : "");



          et_aadhar_number.setText(get_addhaar_num() + "");
          et_address_line_one.setText(get_user_address() + "");
          et_address_line_two.setText(get_sub_distric() + ", " + get_district() + ", " + get_user_state());
          et_city.setText(get_district() + "");
          et_state.setText(get_user_state() + "");
          et_pin_code.setText(get_pincode() + "");


        } else {

        }


      } catch (JSONException e) {
        e.printStackTrace();
      }


    }

  }

  public Object get_user_name() {
    return _user_name;
  }

  public void set_user_name(Object _user_name) {
    this._user_name = _user_name;
  }

  public Object get_gender() {
    return _gender;
  }

  public void set_gender(Object _gender) {
    this._gender = _gender;
  }

  public Object get_user_district() {
    return _user_district;
  }

  public void set_user_district(Object _user_district) {
    this._user_district = _user_district;
  }

  public Object get_user_village_town() {
    return _user_village_town;
  }

  public void set_user_village_town(Object _user_village_town) {
    this._user_village_town = _user_village_town;
  }

  public Object get_user_dob() {
    return _user_dob;
  }

  public void set_user_dob(Object _user_dob) {
    this._user_dob = _user_dob;
  }

  public String get_sub_distric() {
    return _sub_distric;
  }

  public void set_sub_distric(String _sub_distric) {
    this._sub_distric = _sub_distric;
  }

  public String get_user_state() {
    return _user_state;
  }

  public void set_user_state(String _user_state) {
    this._user_state = _user_state;
  }

  public String get_user_address() {
    return _user_address;
  }

  public void set_user_address(String _user_address) {
    this._user_address = _user_address;
  }

  public String get_pan_number() {
    return _pan_number;
  }

  public void set_pan_number(String _pan_number) {
    this._pan_number = _pan_number;
  }

  public String get_district() {
    return _district;
  }

  public void set_district(String _district) {
    this._district = _district;
  }

  public String get_addhaar_num() {
    return _addhaar_num;
  }

  public void set_addhaar_num(String _addhaar_num) {
    this._addhaar_num = _addhaar_num;
  }

  public String get_pincode() {
    return _pincode;
  }

  public void set_pincode(String _pincode) {
    this._pincode = _pincode;
  }
}
