package w.in.kycdemoapp;

import android.app.Application;
import android.util.DisplayMetrics;

import com.androidnetworking.AndroidNetworking;


/**
 * Created by BOX on 19-01-2018.
 */

public class BaseApplication extends Application {

  public static BaseApplication application_context = null;

  public static synchronized BaseApplication getApplicationInstance() {
    return application_context;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    application_context = this;

    AndroidNetworking.initialize(this);
    DisplayMetrics displayMetrics = new DisplayMetrics();

  }
}
