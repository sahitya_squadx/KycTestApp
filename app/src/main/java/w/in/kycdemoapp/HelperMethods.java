package w.in.kycdemoapp;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONObject;


public class HelperMethods {

  private static int screenWidth = 0;
  private static int screenHeight = 0;


  //@Get the Api key from sharePrefrences


  //@To Check if the os version is Marshmallow
  public static boolean isMarshmallow() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
  }

  //@To check if the os version si Lollipop
  public static boolean isLollipop() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
  }


  //@To check if the os version si JellyBean
  public static boolean isJellyBeanMR2() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2;
  }

  //@To check if the os version si JellyBean
  public static boolean isJellyBean() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
  }

  //@To check if the os version si JellyBean
  public static boolean isJellyBeanMR1() {
    return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1;
  }

  //@To get the height of the actionbar


  //@To get the screenHeight
  public static int getScreenHeight(Context c) {
    if (screenHeight == 0) {
      WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
      Display display = wm.getDefaultDisplay();
      Point size = new Point();
      display.getSize(size);
      screenHeight = size.y;
    }

    return screenHeight;
  }

  // To get the
  public static int getScreenWidth(Context c) {
    if (screenWidth == 0) {
      WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
      Display display = wm.getDefaultDisplay();
      Point size = new Point();
      display.getSize(size);
      screenWidth = size.x;
    }

    return screenWidth;
  }


  //@To convert dp into pixel
  public static int dpToPx(int dp) {
    return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
  }


  public static boolean isConnectedToInternet(Context context) {

    ConnectivityManager
            cm = (ConnectivityManager) context
            .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    return activeNetwork != null
            && activeNetwork.isConnectedOrConnecting();

  }

  public static void showToastbar(Context context, String s) {

    Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
  }


  public static void save_user_details_in_pref(JSONObject responsejsonbject) {

  }


  public static Bitmap generateCircularBitmap(Bitmap input) {

    final int width = input.getWidth();
    final int height = input.getHeight();
    final Bitmap outputBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

    final Path path = new Path();
    path.addCircle(
            (float) (width / 2)
            , (float) (height / 2)
            , (float) Math.min(width, (height / 2))
            , Path.Direction.CCW
    );

    final Canvas canvas = new Canvas(outputBitmap);
    canvas.clipPath(path);
    canvas.drawBitmap(input, 0, 0, null);
    return outputBitmap;
  }


  @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR1)
  public static int sizeOf(Bitmap data) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
      return data.getRowBytes() * data.getHeight();
    } else {
      return data.getByteCount();
    }
  }

  public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
    int width = image.getWidth();
    int height = image.getHeight();

    float bitmapRatio = (float) width / (float) height;
    if (bitmapRatio > 1) {
      width = maxSize;
      height = (int) (width / bitmapRatio);
    } else {
      height = maxSize;
      width = (int) (height * bitmapRatio);
    }
    return Bitmap.createScaledBitmap(image, width, height, true);
  }


  public static boolean isAppInstalled(String app_name, Context context) {

    PackageManager pm = context.getPackageManager();
    try {
      pm.getPackageInfo(app_name, PackageManager.GET_ACTIVITIES);
      return true;
    } catch (PackageManager.NameNotFoundException e) {
    }
    return false;
  }


  public static int get_percent_offer(String product_orignal_price, String product_mrp_price) {
    int percent_off = ((int) (Math.round(((Float.parseFloat(product_orignal_price) - Float.parseFloat(product_mrp_price)) / Float.parseFloat(product_orignal_price)) * 100)));
    return percent_off;
  }


  private static void takeUserToPlaystore(Context context) {
    Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
    goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
            Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
            Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
    try {
      context.startActivity(goToMarket);
    } catch (ActivityNotFoundException e) {
      context.startActivity(new Intent(Intent.ACTION_VIEW,
              Uri.parse("http://play.google.com/store/apps/details?id=" + context.getPackageName())));
    }
  }


}
