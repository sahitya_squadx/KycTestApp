package w.in.kycdemoapp;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;


import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.LOLLIPOP;


public class ResourcesUtil {
  private static Context context = BaseApplication.getApplicationInstance().getApplicationContext();
  private static Resources.Theme theme = BaseApplication.getApplicationInstance().getApplicationContext().getTheme();

  public static Drawable getDrawableById(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getDrawable(resId, theme) :
            context.getResources().getDrawable(resId);
  }

  public static String getString(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getString(resId) :
            context.getResources().getString(resId);
  }

  public static int getColor(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getColor(resId) :
            context.getResources().getColor(resId);
  }


  public static ColorStateList getColorStateList(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getColorStateList(resId) :
            context.getResources().getColorStateList(resId);
  }


  public static float getDimen(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getDimension(resId) :
            context.getResources().getDimension(resId);
  }

  public static boolean isConnectedToInternet(Context context) {

    ConnectivityManager
            cm = (ConnectivityManager) context
            .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    return activeNetwork != null
            && activeNetwork.isConnectedOrConnecting();

  }

  public static  String taking_device_id(){
     String android_id = Settings.Secure.getString(BaseApplication.getApplicationInstance().getApplicationContext().getContentResolver(),
            Settings.Secure.ANDROID_ID);
     return android_id;
  }
}
