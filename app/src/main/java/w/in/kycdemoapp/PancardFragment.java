package w.in.kycdemoapp;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.rekognition.AmazonRekognition;
import com.amazonaws.services.rekognition.AmazonRekognitionClient;
import com.amazonaws.services.rekognition.model.DetectTextRequest;
import com.amazonaws.services.rekognition.model.DetectTextResult;
import com.amazonaws.services.rekognition.model.Image;
import com.amazonaws.services.rekognition.model.S3Object;
import com.amazonaws.services.rekognition.model.TextDetection;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.S3ClientOptions;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

/**
 * Created by sahitya on 16/4/18.
 */

public class PancardFragment extends Fragment {


  private static final int REQUEST_CODE = 99;
  private final int REQ_CAMERA = 0, REQ_GALLERY = 1;
  private Calendar calendar;
  private boolean isPanImageSelected = false;
  private int day, month, currentYear;
  private File destination;
  private String[] permissions;
  private Bitmap mainBitMap = null;
  private File _image_file_path = null;
  private Bitmap taken_bitmap_image = null;
  private String _key;
  private String _pancard_number = null;
  private ImageView img_v_select_pan;
  private EditText et_full_name, et_pan_number;
  private EditText tv_dob;
  private Button btn_pan_submit;
  private ImageView img_v_pan_selected;
  private String _gender;
  private Object _image_in_base64;


  private TextView sign_up_male_tv;

  private TextView sign_up_female_tv;
  private String _date;
  private boolean is_checking_input_details = false;
  private String _input_user_name;
  private String _input_pan_number;
  private String _input_dob;
  private String _input_gender;
  private boolean _pan_number_status = false;
  private boolean _data_forma_status = false;

  public static boolean validateDateFormat(String input) {
    return input.matches("([0-9]{2})/([0-9]{2})/([0-9]{4})");
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.pancard_fragment_layout, container, false);

    img_v_select_pan = (ImageView) view.findViewById(R.id.img_v_select_pan);
    et_full_name = (EditText) view.findViewById(R.id.et_full_name);
    et_pan_number = (EditText) view.findViewById(R.id.et_pan_number);
    tv_dob = (EditText) view.findViewById(R.id.tv_dob);
    btn_pan_submit = (Button) view.findViewById(R.id.btn_pan_submit);
    img_v_pan_selected = (ImageView) view.findViewById(R.id.img_v_pan_selected);
    sign_up_male_tv = (TextView) view.findViewById(R.id.sign_up_male_tv);
    sign_up_female_tv = (TextView) view.findViewById(R.id.sign_up_female_tv);

    sign_up_male_tv.setOnClickListener(v -> {
      male_selected();
    });
    sign_up_female_tv.setOnClickListener(v -> {
      female_selected();
    });


    permissions = new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    calendar = Calendar.getInstance();

    img_v_select_pan.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        checkPermissions();
      }
    });

    day = calendar.get(Calendar.DAY_OF_MONTH);
    month = calendar.get(Calendar.MONTH);
    currentYear = calendar.get(Calendar.YEAR);


    btn_pan_submit.setOnClickListener(v -> {
      submit_data_to_server();
    });


    return view;
  }

  private void male_selected() {
    sign_up_male_tv.setTextColor(ResourcesUtil.getColor(R.color.gender_selected));
    sign_up_male_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_gender_selected, 0, 0, 0);
    sign_up_female_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_gender, 0, 0, 0);
    sign_up_female_tv.setTextColor(ResourcesUtil.getColor(R.color.black));
    set_gender("Male");
  }

  private void female_selected() {
    sign_up_female_tv.setTextColor(ResourcesUtil.getColor(R.color.gender_selected));
    sign_up_female_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_gender_selected, 0, 0, 0);
    sign_up_male_tv.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check_gender, 0, 0, 0);
    sign_up_male_tv.setTextColor(ResourcesUtil.getColor(R.color.black));
    set_gender("Female");
  }

  private void submit_data_to_server() {

    String username = et_full_name.getText().toString();
    String pan_numebr = et_pan_number.getText().toString();
    String dob = tv_dob.getText().toString();
    String gender = get_gender();

    if (username != null && !username.isEmpty()) {


      if (pan_numebr != null && !pan_numebr.isEmpty()) {

        if (pan_numebr.length() == 10) {
          if (dob != null && !dob.isEmpty()) {


            if (get_gender() != null && !get_gender().isEmpty()) {

              if (ResourcesUtil.isConnectedToInternet(getActivity())) {

                is_checking_input_details = true;

                set_input_user_name(username);
                set_input_pan_number(pan_numebr);
                set_input_dob(dob);
                set_input_gender(gender);

                new UploadImageAsync().execute();

                //save_user_info_to_server(username, pan_numebr, dob, gender);

              } else {
                Toast.makeText(getActivity(), "Please Connect to Internet", Toast.LENGTH_SHORT).show();
              }

            } else {
              Toast.makeText(getActivity(), "Select Gender", Toast.LENGTH_SHORT).show();
            }
          } else {
            Toast.makeText(getActivity(), "Input DOB please", Toast.LENGTH_SHORT).show();
          }


        } else {

          Toast.makeText(getActivity(), "Invalid Pan Number", Toast.LENGTH_SHORT).show();
          tv_dob.setError("Cant be Empty!");

        }

      } else {

        Toast.makeText(getActivity(), "Fill Pan Number", Toast.LENGTH_SHORT).show();
        et_pan_number.setError("Cant be Empty!");
      }


    } else {

      Toast.makeText(getActivity(), "Fill Your name", Toast.LENGTH_SHORT).show();
      et_full_name.setError("Cant be Empty!");

    }


  }

  private void save_user_info_to_server(String username, String pan_numebr, String dob, String gender) {


    ProgressDialog progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
    progressDialog.setCancelable(false);
    progressDialog.show();


    Log.d("Submit Click", "Submit button is clicked");

    Map<String, Object> bodyParamter = new HashMap<>();
    bodyParamter.put("pan_number", pan_numebr);
    bodyParamter.put("pan_user_name", username);
    bodyParamter.put("pan_users_father_name", "---");
    bodyParamter.put("pan_user_gender", gender);
    bodyParamter.put("pan_user_dob", dob);
    bodyParamter.put("user_pan_card_image", get_image_in_base64());
    bodyParamter.put("user_id", ResourcesUtil.taking_device_id() + "");

    if (bodyParamter != null) {
      AndroidNetworking.post("http://ec2-34-224-102-217.compute-1.amazonaws.com:3000/userAdd/userpancard")
              .setContentType("x-www-form-urlencoded")
              .addBodyParameter(bodyParamter)
              .setPriority(Priority.HIGH)
              .build()
              .getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {
                  Log.d("Response ::", " ---> " + response);
                  try {
                    if (response.has("status") && response.getString("status").contentEquals("1")) {

                      String message = response.has("message") ? response.getString("message") : "--";

                      Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

                      if (is_checking_input_details)
                        is_checking_input_details = false;

                      if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();


                    } else {
                      String message = response.has("message") ? response.getString("message") : "--";
                      Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                      if (progressDialog != null && progressDialog.isShowing())
                        progressDialog.dismiss();
                    }

                  } catch (JSONException e) {
                    e.printStackTrace();
                    if (progressDialog != null && progressDialog.isShowing())
                      progressDialog.dismiss();
                  }
                }

                @Override
                public void onError(ANError anError) {
                  Log.d("Error ::", " -----> " + anError.getMessage());
                  Toast.makeText(getActivity(), "Error :: " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                  if (progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                }
              });
    }

  }

  private void checkPermissions() {
    if (((ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
            && (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED))
            && (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {

      show_dialog_to_get_image();

    } else {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        requestPermissions(permissions, REQ_GALLERY);
      }
    }
  }

  private void show_dialog_to_get_image() {


    LayoutInflater inflater = LayoutInflater.from(getActivity());
    View promtView = inflater.inflate(R.layout.dialog_to_select_image, null);


    final Dialog materialDialog = new Dialog(getActivity());

    materialDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
    materialDialog.setContentView(promtView);
    materialDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    materialDialog.setCanceledOnTouchOutside(true);


    Button open_camera_tv = (Button) promtView.findViewById(R.id.open_camera_tv);
    Button open_gallery_tv = (Button) promtView.findViewById(R.id.open_gallery_tv);

    open_camera_tv.setOnClickListener(v -> {
      startScan(ScanConstants.OPEN_CAMERA);
      materialDialog.dismiss();
    });

    open_gallery_tv.setOnClickListener(v -> {
      startScan(ScanConstants.OPEN_MEDIA);
      materialDialog.dismiss();
    });


    materialDialog.setCancelable(true);
    materialDialog.show();

    materialDialog.getWindow()
            .setLayout((int) (HelperMethods.getScreenWidth(getActivity()) * 0.9), ViewGroup.LayoutParams.WRAP_CONTENT);
    materialDialog.getWindow().setGravity(Gravity.CENTER);

  }

  protected void startScan(int preference) {
    Intent intent = new Intent(getActivity(), ScanActivity.class);
    intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
    startActivityForResult(intent, REQUEST_CODE);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {

      String saved_image_path = data.getExtras().getString(ScanConstants.SCANNED_RESULT);


      Log.d("Received apth is ", "::::::::::::::::::::" + saved_image_path);


      _image_file_path = new File(saved_image_path);

      if (_image_file_path != null && _image_file_path.exists()) {
        Log.d("Existance of the file", "It exist" + _image_file_path.getAbsolutePath());
        Bitmap myBitmap = BitmapFactory.decodeFile(_image_file_path.getAbsolutePath());

        taken_bitmap_image = myBitmap;

        set_image_in_base64(get_base64_from_bitmap(taken_bitmap_image));

        img_v_pan_selected.setImageBitmap(taken_bitmap_image);

        new UploadImageAsync().execute();

      } else {
        Log.d("Existance of the file", "It doesn't exist");
      }
    }
  }

  public String get_key() {
    return _key;
  }

  public void set_key(String _key) {
    this._key = _key;
  }

  private int get_random_numer() {
    Random rand = new Random();

    int n = rand.nextInt(50) + 1;
    return n;
  }

  private boolean validPancardNumber(String pan_number) {

    if (pan_number.length() == 10) {
      String regpan = "^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$";

      if (Pattern.matches(regpan, pan_number)) {
        return true;
      }

      return false;
    } else {
      return false;
    }
  }

  public String get_pancard_number() {
    return _pancard_number;
  }

  public void set_pancard_number(String _pancard_number) {
    this._pancard_number = _pancard_number;
  }

  private String isMaleOrFemale(String text) {
    if (text.equalsIgnoreCase("Male"))
      return "Male";
    else if (text.equalsIgnoreCase("Female"))
      return "Female";
    return null;
  }

  public String get_gender() {
    return _gender;
  }

  public void set_gender(String _gender) {
    this._gender = _gender;
  }

  public Object get_image_in_base64() {
    return _image_in_base64;
  }

  public void set_image_in_base64(Object _image_in_base64) {
    this._image_in_base64 = _image_in_base64;
  }

  private String get_base64_from_bitmap(Bitmap bitmap_image) {
    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    bitmap_image.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
    byte[] byteArray = byteArrayOutputStream.toByteArray();
    String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
    return encoded;
  }

  public String get_date() {
    return _date;
  }

  public void set_date(String _date) {
    this._date = _date;
  }

  public String get_input_user_name() {
    return _input_user_name;
  }

  public void set_input_user_name(String _input_user_name) {
    this._input_user_name = _input_user_name;
  }

  public String get_input_pan_number() {
    return _input_pan_number;
  }

  public void set_input_pan_number(String _input_pan_number) {
    this._input_pan_number = _input_pan_number;
  }

  public String get_input_dob() {
    return _input_dob;
  }

  public void set_input_dob(String _input_dob) {
    this._input_dob = _input_dob;
  }

  public String get_input_gender() {
    return _input_gender;
  }

  public void set_input_gender(String _input_gender) {
    this._input_gender = _input_gender;
  }

  private class UploadImageAsync extends AsyncTask<Void, Void, Void> {

    ProgressDialog progressDialog;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
      progressDialog.setCancelable(false);
      progressDialog.show();

    }

    @Override
    protected Void doInBackground(Void... voids) {


      String bucketName = "imagetestocr";
      String keyName = "pan_card" + ResourcesUtil.taking_device_id() + ".jpg";

      set_key(keyName);

      CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
              BaseApplication.getApplicationInstance().getApplicationContext(),
              "us-east-1:8f773262-2583-4750-822d-2d8440cf0f3f", // Identity pool ID
              Regions.US_EAST_1 // Region
      );


      AmazonS3 s3client = new AmazonS3Client(credentialsProvider);
      s3client.setS3ClientOptions(S3ClientOptions.builder().setPathStyleAccess(true).disableChunkedEncoding().build());


      try {
        AmazonS3Client s3Client = new AmazonS3Client(credentialsProvider);

        PutObjectRequest por = new PutObjectRequest(bucketName,
                keyName, _image_file_path);

        por.setCannedAcl(CannedAccessControlList.PublicRead);
        s3Client.putObject(por);
        Log.d(PancardFragment.class.getSimpleName() + ":: ", "Image is uploaded Successfully");


        if (progressDialog != null && progressDialog.isShowing()) {
          progressDialog.dismiss();
        }


      } catch (AmazonServiceException ase) {
        System.out.println("Caught an AmazonServiceException, which " +
                "means your request made it " +
                "to Amazon S3, but was rejected with an error response" +
                " for some reason.");
        System.out.println("Error Message:    " + ase.getMessage());
        System.out.println("HTTP Status Code: " + ase.getStatusCode());
        System.out.println("AWS Error Code:   " + ase.getErrorCode());
        System.out.println("Error Type:       " + ase.getErrorType());
        System.out.println("Request ID:       " + ase.getRequestId());

      } catch (AmazonClientException ace) {
        System.out.println("Caught an AmazonClientException, which " +
                "means the client encountered " +
                "an internal error while trying to " +
                "communicate with S3, " +
                "such as not being able to access the network.");
        System.out.println("Error Message: " + ace.getMessage());
      }
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      Log.d(PancardFragment.class.getSimpleName() + ":: ", "Uploading Image process Completed");
      new GettingDataFromImage().execute();
    }
  }

  private class GettingDataFromImage extends AsyncTask<Void, Void, Void> {
    ProgressDialog progressDialog;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      progressDialog = new ProgressDialog(getActivity(), R.style.MyTheme);
      progressDialog.setCancelable(false);
      progressDialog.show();

      Log.d(PancardFragment.class.getSimpleName() + "", "Text Extraction of the Image Started....");
    }

    @Override
    protected Void doInBackground(Void... voids) {

      String bucket = "imagetestocr";

      CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
              BaseApplication.getApplicationInstance().getApplicationContext(),
              "us-east-1:8f773262-2583-4750-822d-2d8440cf0f3f", // Identity pool ID
              Regions.US_EAST_1 // Region
      );


      AmazonRekognition rekognitionClient = new AmazonRekognitionClient(credentialsProvider);


      DetectTextRequest request = new DetectTextRequest()
              .withImage(new Image()
                      .withS3Object(new S3Object()
                              .withName(get_key())
                              .withBucket(bucket)));


      try {
        DetectTextResult result = rekognitionClient.detectText(request);
        List<TextDetection> textDetections = result.getTextDetections();


        for (TextDetection text : textDetections) {

          Log.d("Data in image::", "---> " + text.getDetectedText() + "\n"
                  + "---> " + "Confidence: " + text.getConfidence().toString() + "\n" +
                  "--->Id :  + text.getId()");


          if (is_checking_input_details) {

            if (validPancardNumber(text.getDetectedText())) {
              if (get_input_pan_number().contentEquals(text.getDetectedText())) {
                _pan_number_status = true;

              } else {

                _pan_number_status = false;
              }
            }

            if (validateDateFormat(text.getDetectedText())) {
              if (get_input_dob().contentEquals(text.getDetectedText())) {
                _data_forma_status = true;
              } else {
                _data_forma_status = false;

              }

            }


          } else {
            if (validPancardNumber(text.getDetectedText())) {
              set_pancard_number(text.getDetectedText());
            }

            if (validateDateFormat(text.getDetectedText())) {
              set_date(text.getDetectedText());
            }

            if (isMaleOrFemale(text.getDetectedText()) != null) {
              set_gender(isMaleOrFemale(text.getDetectedText()));
            }
          }


          String data_from_image = "Detected: " + text.getDetectedText() +
                  "\nConfidence: " + text.getConfidence().toString() +
                  "\nId : " + text.getId() +
                  "\nParent Id: " + text.getParentId() +
                  "\nType: " + text.getType();

          Log.d(PancardFragment.class.getSimpleName() + "", "Data >>> " + data_from_image);
        }


        if (progressDialog != null && progressDialog.isShowing()) {
          progressDialog.dismiss();
        }


      } catch (Exception e) {
        e.printStackTrace();
        Log.d("Exception is", ":: " + e.getMessage());
      }
      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);
      Log.d(PancardFragment.class.getSimpleName() + "", "Text Extraction from Image is done");
      if (is_checking_input_details) {
        if (!_pan_number_status)
          Toast.makeText(getActivity(), "Your Input Pan number is incorrect as of on Document", Toast.LENGTH_SHORT).show();
        if (!_data_forma_status)
          Toast.makeText(getActivity(), "Your Input DOB is incorrect as of on Document", Toast.LENGTH_SHORT).show();

        if (_pan_number_status && _data_forma_status) {
          save_user_info_to_server(get_input_user_name(), get_input_pan_number(), get_input_dob(), get_input_gender());
        }

      }

      et_pan_number.setText(get_pancard_number());
      tv_dob.setText(get_date() + "");
    }
  }


}
