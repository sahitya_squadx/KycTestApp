package w.in.kycdemoapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;

import java.util.List;

import info.androidhive.barcode.BarcodeReader;

public class ScanActivity extends AppCompatActivity implements BarcodeReader.BarcodeReaderListener {

  BarcodeReader barcodeReader;
  private Intent recieved_intent = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_scan);

    getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    // get the barcode reader instance
    barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
    recieved_intent = getIntent();

  }


  @Override
  public void onScanned(Barcode barcode) {
    barcodeReader.playBeep();
    Log.d("Checking data", ":::::::::: " + barcode.displayValue);
    recieved_intent.putExtra("received_text", barcode.displayValue + "");
    setResult(RESULT_OK, recieved_intent);
    //Toast.makeText(this, "Scanned QR Successfully", Toast.LENGTH_SHORT).show();
    finish();
  }

  @Override
  public void onScannedMultiple(List<Barcode> list) {

  }

  @Override
  public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

  }

  @Override
  public void onScanError(String s) {
    Toast.makeText(getApplicationContext(), "Error occurred while scanning " + s, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onCameraPermissionDenied() {
    finish();
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
      finish();
    }
    return super.onOptionsItemSelected(item);
  }
}
