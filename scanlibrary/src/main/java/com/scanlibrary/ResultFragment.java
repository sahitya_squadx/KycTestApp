package com.scanlibrary;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by jhansi on 29/03/15.
 */
public class ResultFragment extends Fragment {

  private static ProgressDialogFragment progressDialogFragment;
  private View view;
  private ImageView scannedImageView;
  private Button doneButton;
  private Bitmap original;
  private Button originalButton;
  private Button MagicColorButton;
  private Button grayModeButton;
  private Button bwButton;
  private Bitmap transformed;

  public ResultFragment() {
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    view = inflater.inflate(R.layout.result_layout, null);
    init();
    return view;
  }

  private void init() {
    scannedImageView = (ImageView) view.findViewById(R.id.scannedImage);
    originalButton = (Button) view.findViewById(R.id.original);
    originalButton.setOnClickListener(new OriginalButtonClickListener());
    MagicColorButton = (Button) view.findViewById(R.id.magicColor);
    MagicColorButton.setOnClickListener(new MagicColorButtonClickListener());
    grayModeButton = (Button) view.findViewById(R.id.grayMode);
    grayModeButton.setOnClickListener(new GrayButtonClickListener());
    bwButton = (Button) view.findViewById(R.id.BWMode);
    bwButton.setOnClickListener(new BWButtonClickListener());
    Bitmap bitmap = getBitmap();
    setScannedImage(bitmap);
    doneButton = (Button) view.findViewById(R.id.doneButton);
    doneButton.setOnClickListener(new DoneButtonClickListener());
  }

  private Bitmap getBitmap() {
    Uri uri = getUri();
    try {
      original = Utils.getBitmap(getActivity(), uri);
      getActivity().getContentResolver().delete(uri, null, null);
      return original;
    } catch (IOException e) {
      e.printStackTrace();
    }
    return null;
  }

  private Uri getUri() {
    Uri uri = getArguments().getParcelable(ScanConstants.SCANNED_RESULT);
    return uri;
  }

  public void setScannedImage(Bitmap scannedImage) {
    scannedImageView.setImageBitmap(scannedImage);
  }

  protected synchronized void showProgressDialog(String message) {
    if (progressDialogFragment != null && progressDialogFragment.isVisible()) {
      // Before creating another loading dialog, close all opened loading dialogs (if any)
      progressDialogFragment.dismissAllowingStateLoss();
    }
    progressDialogFragment = null;
    progressDialogFragment = new ProgressDialogFragment(message);
    FragmentManager fm = getFragmentManager();
    progressDialogFragment.show(fm, ProgressDialogFragment.class.toString());
  }

  protected synchronized void dismissDialog() {
    progressDialogFragment.dismissAllowingStateLoss();
  }

  private String saveToInternalStorage(Bitmap bitmapImage) {
    ContextWrapper cw = new ContextWrapper(getActivity().getApplicationContext());
    File directory = cw.getDir("Ezopay", Context.MODE_PRIVATE);
    // Create imageDir
    File mypath = new File(directory, System.currentTimeMillis() + ".jpg");
    FileOutputStream fos = null;
    try {
      fos = new FileOutputStream(mypath);
      bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, fos);
    } catch (Exception e) {
      e.printStackTrace();
    } finally {
      try {
        fos.close();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return mypath.getAbsolutePath();
  }

  private class DoneButtonClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
      showProgressDialog(getResources().getString(R.string.loading));
      AsyncTask.execute(new Runnable() {
        @Override
        public void run() {
          try {
            Intent data = new Intent();
            Bitmap bitmap = transformed;
            if (bitmap == null) {
              bitmap = original;
            }

            String absolutePath = saveToInternalStorage(bitmap);

//            Uri uri = Utils.getUri(getActivity(), bitmap);
//            Log.d("Saving after change", "<><><><>><><>><<><><> :: " + uri.getPath());
            data.putExtra(ScanConstants.SCANNED_RESULT, absolutePath);
            getActivity().setResult(Activity.RESULT_OK, data);
            original.recycle();
            System.gc();
            getActivity().runOnUiThread(new Runnable() {
              @Override
              public void run() {
                dismissDialog();
                getActivity().finish();
              }
            });
          } catch (Exception e) {
            e.printStackTrace();
          }
        }
      });
    }


  }

  private class BWButtonClickListener implements View.OnClickListener {
    @Override
    public void onClick(final View v) {
      showProgressDialog(getResources().getString(R.string.applying_filter));
      AsyncTask.execute(new Runnable() {
        @Override
        public void run() {
          try {
            transformed = ((ScanActivity) getActivity()).getBWBitmap(original);
          } catch (final OutOfMemoryError e) {
            getActivity().runOnUiThread(new Runnable() {
              @Override
              public void run() {
                transformed = original;
                scannedImageView.setImageBitmap(original);
                e.printStackTrace();
                dismissDialog();
                onClick(v);
              }
            });
          }
          getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
              scannedImageView.setImageBitmap(transformed);
              dismissDialog();
            }
          });
        }
      });
    }
  }

  private class MagicColorButtonClickListener implements View.OnClickListener {
    @Override
    public void onClick(final View v) {
      showProgressDialog(getResources().getString(R.string.applying_filter));
      AsyncTask.execute(new Runnable() {
        @Override
        public void run() {
          try {
            transformed = ((ScanActivity) getActivity()).getMagicColorBitmap(original);
          } catch (final OutOfMemoryError e) {
            getActivity().runOnUiThread(new Runnable() {
              @Override
              public void run() {
                transformed = original;
                scannedImageView.setImageBitmap(original);
                e.printStackTrace();
                dismissDialog();
                onClick(v);
              }
            });
          }
          getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
              scannedImageView.setImageBitmap(transformed);
              dismissDialog();
            }
          });
        }
      });
    }
  }

  private class OriginalButtonClickListener implements View.OnClickListener {
    @Override
    public void onClick(View v) {
      try {
        showProgressDialog(getResources().getString(R.string.applying_filter));
        transformed = original;
        scannedImageView.setImageBitmap(original);
        dismissDialog();
      } catch (OutOfMemoryError e) {
        e.printStackTrace();
        dismissDialog();
      }
    }
  }

  private class GrayButtonClickListener implements View.OnClickListener {
    @Override
    public void onClick(final View v) {
      showProgressDialog(getResources().getString(R.string.applying_filter));
      AsyncTask.execute(new Runnable() {
        @Override
        public void run() {
          try {
            transformed = ((ScanActivity) getActivity()).getGrayBitmap(original);
          } catch (final OutOfMemoryError e) {
            getActivity().runOnUiThread(new Runnable() {
              @Override
              public void run() {
                transformed = original;
                scannedImageView.setImageBitmap(original);
                e.printStackTrace();
                dismissDialog();
                onClick(v);
              }
            });
          }
          getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
              scannedImageView.setImageBitmap(transformed);
              dismissDialog();
            }
          });
        }
      });
    }
  }
}